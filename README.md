# Game Physics - Assignment 2 🏃‍♀️🔥

![Gameplay](GamePhysics_BG2.png)

*Note: This game uses Unreal Engine 4 (Blueprint only).*

It's a simple 3D side-scrolling game with physics-based puzzles.
The player has to solve the puzzles and reach the end of the map to win.
The player can cast wind to manipulate with objects, e.g. pushing objects.

### Win Condition
Reach the end of the map

### Lose Condition
Dies to the fire trap

## Controls
  Keys	  |   In-game action
-------   | ------------------------------
   ↑ / W	    |   Move forward
   ↓ / S	    |   Move backward
   ← / A	    |   Move left
   → / D	    |   Move right 
  Space   |	 Jump
  Left mouse       |	 Cast wind


## ❓How to install our game❓
1. Open Unreal Engine
2. Select the project
3. Click the Play button at the top toolbar

## Features
###  Sound effect
- Player actions 
- When puzzle is completed
- Coin collection

### Animation
- Idle animation
- Walk animation
- Jump animation
- Casting wind animation
- Death animation
- Win animation

### Voicelines
- Start game
- Win game
- Idle

### Particle effect
- Coin collection
- Cast wind (by player)
- Fire trap
- Magic barrel


#### Please check out our [documentation](https://github.com/mich0292/Game_Physics/blob/6957b9effd1935083e4262de4c4bd5b76ef51d75/(1171100973_1171101517)%20WrittenReport.pdf) for more details! 😀

##### *Note: This is a simple game that me and my assignment partner did in less than one week.*
